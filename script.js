/*Теоретичні питання.
1. Події - це дії, які користувач може зробити на сторінці сайту. До подій відносяться такі дії, як наведення, скролл,
виділення якоїсь частини тексту, або іншого елементу сторінки, натискання клавіатури, кліки мишкі, надсилання форми і т.д.
Події можуть відноситись не тільки до DOM, але і до CSS, наприклад "transitionend" - це завершення анімації.
2. Основні події миші в js:
 - "click" - коли користувач клікає на елемент мишкою (на сенсорних пристроях це генерується при торканні);
 - "contextmenu" - користувач правою кнопкою миші клікає на елемент;
 - "mouseover - курсор миші наводиться на елемент;
 - "mouseout" -  курсор миші залишає елемент;
 - "mousedown" - коли кнопка миші натискана;
 - "mouseup" -  кнопка миші відпускається;
 - "mousemove" - коли миша рухається;
 - "dblclick" - подвійний клік мишкою.
Наприклад призначаємо обробник події через "onclick". В даному випадку при клікі мишкою, код всередені "onclick" буде
виконуватись: <input value = "Click me!!!" onclick = "alert('Click!!!')" type = "button">
Також можно використати функцію:
<script> function click() {
    alert('Click!!!');
    }
</script>
<input value = "Click me!!!" onclick = "click()" type = "button">
Ці варіанти обробки подій не можуть добавити нові обробники подій, треба буде змінювати написаний код, тому існує
третій варіант - метод "addEventListener()":
<input class= "btn" value = "Click me!!!" type = "button">
   const btn = document.querySelector(".btn");
   btn.addEventListener("click", function myClick() {
    alert('Click!!!');
    });
3. Подія "contextmenu" - це клик правою кнопкою миші користувачем, вона викликає контекстне меню. Контекстне меню - меню,
 що з'являється при взаємодії користувача з графічним інтерфейсом (при натисканні правої кнопки миші). У браузері при натисканні
 правою кнопкою миші спрацьовує подія "contextmenu".
 Метод .contextmenu() прив'язує JavaScript обробник подій "contextmenu" (виклик контекстного меню на елементі - клік правою
 кнопкою миші) або запускає цю подію на обраний елемент.
*/
"use strict"
const btn = document.getElementById("btn-click");
const section = document.getElementById("content");
btn.addEventListener("click", () => {
   const newElement = document.createElement("p");
   newElement.textContent = "New Paragraph";
   section.prepend(newElement);
},
    {once: true});

const newButton = document.createElement("button");
newButton.id = "btn-input-create";
newButton.textContent = "New click"
section.append(newButton);
newButton.addEventListener("click", function (){
    const newInput = document.createElement("input");
    newInput.type = "text";
    newInput.name = "input";
    newInput.placeholder = "Enter the data";
    section.append(newInput);
},
    {once: true});

 newButton.style.display = "block";
 newButton.style.margin = "0 auto";









